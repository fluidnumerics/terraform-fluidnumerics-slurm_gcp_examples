# Fluid-Slurm-GCP

This example deployment creates a basic fluid-slurm-gcp instance with CentOS7 operating system

## Using this example

### Configuring permissions
This example uses modules from Fluid Numerics' GSR repositories. Make sure you have installed and initialized the gcloud SDK. Then,
```
git config --global credential.'https://source.developers.google.com'.helper gcloud.sh
terraform init
```


### Required API's
```
servicenetworking.googleapis.com
compute.googleapis.com
```
