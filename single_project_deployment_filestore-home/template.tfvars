
parent_folder = ""
slurm_gcp_admins = ["group:support@domain.com"]
slurm_gcp_users = ["user:user@domain.com"]

//filestore_name = "fluid-home-server"
//filestore_tier = "STANDARD"
//filestore_capacity_gb = 2048

filestore_zone = "zone"
filestore_project = "networking-project-id"
filestore_network = "fluid-network"

slurm_gcp_name = "fluid"
controller = {
  machine_type = "n1-standard-4",
  disk_size_gb = 15,
  disk_type = "pd-standard",
  labels = {"slurm-gcp":"controller"},
  project = "gcp-project",
  region = "region",
  vpc_subnet = "projects/networking-project-id/regions/region/subnetworks/subnetwork",
  zone = "zone"
}

default_partition = "demo"

login = [
  {
    machine_type = "n1-standard-4",
    disk_size_gb = 15,
    disk_type = "pd-standard",
    labels = {"slurm-gcp":"login"},
    project = "gcp-project",
    region = "region",
    vpc_subnet = "projects/networking-project-id/regions/region/subnetworks/subnetwork"
    zone = "zone"
  }
]

partitions = [
  {
    name = "demo",
    project = "gcp-project",
    labels = {"slurm-gcp":"demo"},
    max_time = "1:00:00",
    machines = [
      {
        name = "demo-a",
        disk_size_gb = 15,
        disk_type = "pd-standard",
        disable_hyperthreading = false,
        external_ip = false,
        gpu_count = 0,
        gpu_type = "",
        n_local_ssds = 0,
        local_ssd_mount_directory = "/scratch",
        machine_type = "n1-standard-2",
        max_node_count = 10,
        preemptible_bursting = false,
        static_node_count = 0,
        vpc_subnet = "projects/networking-project-id/regions/region/subnetworks/subnetwork"
        zone = "zone"
      }
    ]
  }
]

slurm_accounts = [
  {
    name = "demo-users",
    users = ["user"],
    allowed_partitions = ["demo"]
  }
]


