
parent_folder = "folders/146798967382"
slurm_gcp_admins = ["group:support@fluidnumerics.com"]
slurm_gcp_users = ["user:joe@fluidnumerics.com"]

slurm_gcp_name = "fluid"
vpc_network = "projects/fluid-faha081j/global/networks/default"
controller = {
  machine_type = "n1-standard-2",
  disk_size_gb = 15,
  disk_type = "pd-standard",
  labels = {"slurm-gcp":"controller"},
  project = "fluid-faha081j",
  region = "us-central1",
  vpc_subnet = "projects/fluid-faha081j/regions/us-central1/subnetworks/default",
  zone = "us-central1-a"
}

default_partition = "demo"

login = [
  {
    machine_type = "n1-standard-2",
    disk_size_gb = 15,
    disk_type = "pd-standard",
    labels = {"slurm-gcp":"login"},
    project = "fluid-faha081j",
    region = "us-central1",
    vpc_subnet = "projects/fluid-faha081j/regions/us-central1/subnetworks/default"
    zone = "us-central1-a"
  }
]

partitions = [
  {
    name = "demo",
    project = "fluid-faha081j",
    labels = {"slurm-gcp":"demo"},
    max_time = "1:00:00",
    machines = [
      {
        name = "demo-a",
        disk_size_gb = 15,
        disk_type = "pd-standard",
        disable_hyperthreading = false,
        external_ip = false,
        gpu_count = 0,
        gpu_type = "",
        n_local_ssds = 0,
        local_ssd_mount_directory = "/scratch",
        machine_type = "n1-standard-8",
        max_node_count = 10,
        preemptible_bursting = false,
        static_node_count = 0,
        vpc_subnet = "projects/fluid-faha081j/regions/us-central1/subnetworks/default"
        zone = "us-central1-a"
      }
    ]
  }
]

slurm_accounts = [
  {
    name = "demo-users",
    users = ["joe"],
    allowed_partitions = ["demo"]
  }
]


