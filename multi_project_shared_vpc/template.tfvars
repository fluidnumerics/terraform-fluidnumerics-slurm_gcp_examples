parent_folder = "folders/FOLDER-ID"


// ******** Networking Settings ******** //
shared_vpc_network = "fluid-network"

shared_vpc_subnetworks = [
  {
    name = "fluid-subnet-CONTROLLER_REGION"
    cidr = "10.10.0.0/16"
    description = "Subnetwork for CONTROLLER_REGION"
    private_ip_google_access = true
    region = "CONTROLLER_REGION"
  },
  {
    name = "fluid-subnet-COMPUTE_REGION"
    cidr = "10.11.0.0/16"
    description = "Subnetwork for COMPUTE_REGION"
    private_ip_google_access = true
    region = "COMPUTE_REIGON"
  }
]

firewall_rules = [
  {
    name = "fluid-cluster-allow-ssh"
    whitelist_ips = []
    whitelist_tags = []
    target_tags = []
    allow = [
      {
        protocol = "tcp"
        ports = ["22"]
      },
      {
        protocol = "icmp"
        ports = []
      }
    ]
  },
  {
    name = "fluid-cluster-allow-internal"
    whitelist_ips = ["10.10.0.0/16","10.11.0.0/16"]
    whitelist_tags = []
    target_tags = []
    allow = [
      {
        protocol = "tcp"
        ports = ["0-65535"]
      },
      {
        protocol = "udp"
        ports = ["0-65535"]
      },
      {
        protocol = "icmp"
        ports = []
      }
    ]
  }
]


// ******** Fluid-Slurm-GCP Settings ******** //

slurm_gcp_admins = ["group:support@domain.com"]
slurm_gcp_users = ["user:user@domain.com"]

slurm_gcp_name = "fluid"
controller = {
  machine_type = "n1-standard-4",
  disk_size_gb = 15,
  disk_type = "pd-standard",
  labels = {"slurm-gcp":"controller"},
  project = "CONTROLLER_PROJECT_ID",
  region = "CONTROLLER_REGION",
  vpc_subnet = "projects/CONTROLLER_PROJECT_ID/regions/CONTROLLER_REGION/subnetworks/fluid-subnetwork-CONTROLLER_REGION",
  zone = "CONTROLLER_ZONE"
}

default_partition = "demo"

login = [
  {
    machine_type = "n1-standard-4",
    disk_size_gb = 15,
    disk_type = "pd-standard",
    labels = {"slurm-gcp":"login"},
    project = "CONTROLLER_PROJECT_ID",
    region = "CONTROLLER_REGION",
    vpc_subnet = "projects/CONTROLLER_PROJECT_ID/regions/CONTROLLER_REGION/subnetworks/fluid-subnetwork-CONTROLLER_REGION",
    zone = "CONTROLLER_ZONE"
  }
]

partitions = [
  {
    name = "demo",
    project = "COMPUTE_PROJECCT",
    labels = {"slurm-gcp":"demo"},
    max_time = "1:00:00",
    machines = [
      {
        name = "demo-a",
        disk_size_gb = 15,
        disk_type = "pd-standard",
        disable_hyperthreading = false,
        external_ip = false,
        gpu_count = 0,
        gpu_type = "",
        n_local_ssds = 0,
        local_ssd_mount_directory = "/scratch",
        machine_type = "n1-standard-2",
        max_node_count = 10,
        preemptible_bursting = false,
        static_node_count = 0,
        vpc_subnet = "projects/CONTROLLER_PROJECT_ID/regions/COMPUTE_REGION/subnetworks/fluid-subnetwork-COMPUTE_REGION",
        zone = "COMPUTE_ZONE"
      }
    ]
  }
]

slurm_accounts = [
  {
    name = "demo-users",
    users = ["user"],
    allowed_partitions = ["demo"]
  }
]


