

// Configure the Google Cloud provider
provider "google" {
 version = "3.9"
}

// Create the filestore instance
resource "google_filestore_instance" "home_server" {
  name = var.filestore_name
  zone = var.filestore_zone
  tier = var.filestore_tier
  project = var.filestore_project

  file_shares {
    capacity_gb = var.filestore_capacity_gb
    name        = "home"
  }

  networks {
    network = var.filestore_network
    modes   = ["MODE_IPV4"]
  }
}


// Set the filestore instance as the `/home` mount
locals {
  mounts = [{group = "root",
             mount_directory = "/home",
             mount_options = "rw,hard,intr",
             owner = "root",
             protocol = "nfs"
             permission = "755"
             server_directory = "${google_filestore_instance.home_server.networks[0].ip_addresses[0]}:/home"
           }]
}

// Create the Slurm-GCP cluster
module "slurm_gcp" {
  source = "git::https://source.developers.google.com/p/managed-fluid-slurm-gcp/r/terraform-fluidnumerics-slurm_gcp?ref=v1.0.9"
  parent_folder = var.parent_folder
  slurm_gcp_admins = var.slurm_gcp_admins
  slurm_gcp_users = var.slurm_gcp_users
  name = var.slurm_gcp_name
  tags = var.slurm_gcp_tags
  controller = var.controller
  login = var.login
  mounts = local.mounts
  partitions = var.partitions
  slurm_accounts = var.slurm_accounts
}
