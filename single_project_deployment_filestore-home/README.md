# Fluid-Slurm-GCP + Filestore

This example deployment creates a filestore instance and uses it to serve `/home` on a fluid-slurm-gcp cluster.

## Using this example
