// Configure the Google Cloud provider
provider "google" {
 version = "3.9"
}

module "gcp_shared_networking" {
  source = "git::https://source.developers.google.com/p/managed-fluid-slurm-gcp/r/terraform-fluidnumerics-gcp_shared_networking?ref=v0.0.4"
  firewall_rules = var.firewall_rules
  shared_vpc_host_project = var.controller.project
  shared_vpc_network = var.shared_vpc_network
  shared_vpc_service_projects = var.shared_vpc_service_projects
  shared_vpc_subnetworks = var.shared_vpc_subnetworks
  gcp_projects_list = [""]
}

// Create the Slurm-GCP cluster
module "slurm_gcp" {
  source = "git::https://source.developers.google.com/p/managed-fluid-slurm-gcp/r/terraform-fluidnumerics-slurm_gcp?ref=v1.0.9"
  parent_folder = var.parent_folder
  slurm_gcp_admins = var.slurm_gcp_admins
  slurm_gcp_users = var.slurm_gcp_users
  name = var.slurm_gcp_name
  tags = var.slurm_gcp_tags
  controller = var.controller
  login = var.login
  mounts = var.mounts
  partitions = var.partitions
  slurm_accounts = var.slurm_accounts
}

