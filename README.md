# Fluid-Slurm-GCP : Terraform Examples
Copyright 2020, Fluid Numerics LLC

Thie repository provides examples for using Fluid Numerics' private terraform modules to create your own custom fluid-slurm-gcp Cloud-HPC systems.


## Costs and Access
Fluid-Slurm-GCP images incur a usage fee of $0.01 USD/vCPU/hour and $0.09/GPU/hour for controller, login, and compute instances.

`app.terraform.io/fluidnumerics/slurm_gcp/fluidnumerics` is a private Terraform Cloud module hosted by Fluid Numerics.
Reach out to support@fluidnumerics.com to learn more about getting access to this and other Terraform modules.

