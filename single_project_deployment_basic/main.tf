

// Configure the Google Cloud provider
provider "google" {
 version = "3.9"
}

// Create the Slurm-GCP cluster
module "slurm_gcp" {
  source = "git::https://source.developers.google.com/p/managed-fluid-slurm-gcp/r/terraform-fluidnumerics-slurm_gcp?ref=v1.0.9"
  parent_folder = var.parent_folder
  slurm_gcp_admins = var.slurm_gcp_admins
  slurm_gcp_users = var.slurm_gcp_users
  name = var.slurm_gcp_name
  tags = var.slurm_gcp_tags
  controller = var.controller
  login = var.login
  mounts = var.mounts
  partitions = var.partitions
  slurm_accounts = var.slurm_accounts
  controller_image = "projects/fluid-cluster-ops/global/images/fluid-slurm-gcp-controller-centos-rc2-4-0-b"
  login_image = "projects/fluid-cluster-ops/global/images/fluid-slurm-gcp-login-centos-rc2-4-0-b"
  compute_image = "projects/fluid-cluster-ops/global/images/fluid-slurm-gcp-compute-centos-rc2-4-0-b"
}

